// Fill out your copyright notice in the Description page of Project Settings.


#include "MyCharacter.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Camera/CameraComponent.h"
#include "ConstructorHelpers.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Character.h"
#include "RotationMatrix.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/SceneComponent.h"
#include "Animation/AnimMontage.h"
#include "AnimNotifyQueue.h"
#include "Weapon/Base_Weapon.h"
#include "Animation/AnimInstance.h"
#include "Engine/World.h"
#include "Vitality/VitalityComponent.h"
#include "GameFramework/Pawn.h"

// Sets default values
AMyCharacter::AMyCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SpringArmComponent = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArmComponent->SetupAttachment(RootComponent);
		
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComponent->SetupAttachment(SpringArmComponent);

	static ConstructorHelpers::FObjectFinder<USkeletalMesh> SkeletalMesh(TEXT("SkeletalMesh'/Game/AnimStarterPack/UE4_Mannequin/Mesh/SK_Mannequin.SK_Mannequin'"));

	if (SkeletalMesh.Succeeded())
	{
		GetMesh()->SetSkeletalMesh(SkeletalMesh.Object);
	}

	GetMesh()->AddLocalTransform(FTransform(FRotator(0, -90, 0).Quaternion(), FVector(0, 0, -90)));
			
	bUseControllerRotationYaw = false;

	SpringArmComponent->bInheritPitch = false;
	SpringArmComponent->bInheritRoll = false;
	SpringArmComponent->bInheritYaw = false;

	GetCharacterMovement()->MaxWalkSpeed = MaxWalkSpeed;

	VitalityComponent = CreateDefaultSubobject<UVitalityComponent>(TEXT("VitalityComponent"));

	VitalityComponent->OnOwnerDied.AddDynamic(this, &AMyCharacter::CharacterDied);

	MaxWalkSpeed = 400.0f;
	JogSpeed = 600.0f;
	SprintSpeed = 1000.0f;
	bJog = true;
	Zoom_v = 1000.0f;
	bFirePressed = false;
	bRechargeOn = false;
	bWeaponEquiped = false;
	bWeaponUnEquiped = false;
// 	bDropWeapon = false;

	AmmoPool = 30; 
	LoadedAmmo = AmmoPool;

}
	

// Called when the game starts or when spawned
void AMyCharacter::BeginPlay()
{
	Super::BeginPlay();

	CurrentWeapon = SpawnWeapon();
	AttachWeapon(WeaponSocket);
		
}

void AMyCharacter::MoveForvard(float Scale)
{
	AddMovementInput(FVector(1.0, 0.0, 0.0), Scale);
	

}

void AMyCharacter::MoveRight(float Scale)
{
	AddMovementInput(FVector(0.0, 1.0, 0.0), Scale);
	
}

void AMyCharacter::Jog()
{
	GetCharacterMovement()->MaxWalkSpeed = JogSpeed;
	bJog = true;
}
	
void AMyCharacter::Walk()
{
	GetCharacterMovement()->MaxWalkSpeed = MaxWalkSpeed;
	bJog = false;
}

void AMyCharacter::StartSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
	bJog = true;
}

void AMyCharacter::StopSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = MaxWalkSpeed;
	bJog = false;
}

void AMyCharacter::ZoomIn()
{
	Zoom_v -= 20.0;

	if (Zoom_v <= 800.0)
	{
		SpringArmComponent->TargetArmLength = 800.0;
		Zoom_v = 800.0;
		}
	else
	{
		SpringArmComponent->TargetArmLength = Zoom_v;
	}
}

void AMyCharacter::ZoomOut()
{
	Zoom_v += 20.0;

	if (Zoom_v >= 1200.0)
	{
		SpringArmComponent->TargetArmLength = 1200.0;
		Zoom_v = 1200.0;
		}
	else
	{
		SpringArmComponent->TargetArmLength = Zoom_v;
	}
}

void AMyCharacter::StartFire()
{
	if (LoadedAmmo <= 0)
	{
		return;
	}
	LoadedAmmo = LoadedAmmo - 1;

	OnAmmoPoolChanged.Broadcast();

	if (Fire_Rifle_Hip1_Add_Montage != NULL)
	{
		PlayAnimMontage(Fire_Rifle_Hip1_Add_Montage);
	}
	if (CurrentWeapon)
		{
			CurrentWeapon->StartFire();
		}
	
	bFirePressed = true;
}

void AMyCharacter::StopFire()
{
	if (CurrentWeapon)
	{
		CurrentWeapon->StopFire();
	}

	bFirePressed = false;
}

void AMyCharacter::RechargeOn()
{
	/*Do we have ammo in the AmmoPool?*/
		if (AmmoPool <= 0 || LoadedAmmo >= 30)
		{
			return;
		}
			
		//Do we have enough to meet what the gun needs?
		if (AmmoPool < (30 - LoadedAmmo))
		{
			LoadedAmmo = LoadedAmmo + AmmoPool;
			AmmoPool = 0;

			
		}
		else
		{
			AmmoPool = AmmoPool - (30 - LoadedAmmo);
			LoadedAmmo = 30;
		}
		OnRecharged.Broadcast();

	if (Recharge_Rifle_Hip_Montage != NULL)
	{
		PlayAnimMontage(Recharge_Rifle_Hip_Montage);
	}	
		bRechargeOn = true;
		bFirePressed = false;
		bWeaponEquiped = false;
		bWeaponUnEquiped = false;
}

void AMyCharacter::AttachWeapon(FName SocketName)
{
	if (CurrentWeapon)
	{
		CurrentWeapon->AttachToComponent(GetMesh(), FAttachmentTransformRules::SnapToTargetIncludingScale, SocketName);
		//FireMontage = CurrentWeapon->Fire_Rifle_Hip1_Add_Montage;
	}
	OnWeaponChanged.Broadcast();
}

// void AMyCharacter::DropWeapon()
// {
// 	if (CurrentWeapon)
// 	{
// 		CurrentWeapon->DropWeapon();
// 		bDropWeapon = true;
// 	}
// }

void AMyCharacter::EquipWeapon()
{
	if (EquipWeapon_Add_Montage != NULL)
	{
		PlayAnimMontage(EquipWeapon_Add_Montage);
	}
	bWeaponEquiped = true;
}

void AMyCharacter::UnEquipWeapon()
{
	if (UnEquipWeapon_Add_Montage != NULL)
	{
		PlayAnimMontage(UnEquipWeapon_Add_Montage);
	}
	bWeaponUnEquiped = true;
}

void AMyCharacter::CharacterDied()
{
//	GetController()->UnPossess();
	GetCharacterMovement()->DisableMovement();
	StopFire();
	/*Disable Fire After Death*/
	CurrentWeapon = nullptr;

	UE_LOG(LogTemp, Warning, TEXT("Char is Dead"));
}

ABase_Weapon* AMyCharacter::SpawnWeapon()
{
	if (WeaponToSpawn_Class)
	{
		FTransform TmpTransform = FTransform::Identity;
		FActorSpawnParameters SpawnParams;
		SpawnParams.Owner = this;
		
		UE_LOG(LogTemp, Warning, TEXT("Trying to spawn weapon"));
		return Cast<ABase_Weapon>(GetWorld()->SpawnActor(WeaponToSpawn_Class, &TmpTransform, SpawnParams));
	}
	
	return nullptr;
}


// Called every frame
void AMyCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	RotateToMouse();
		  
}

void AMyCharacter::RotateToMouse()
{
	FHitResult Hit;

	if (!VitalityComponent->IsAlive())
	{
		return;
	}


	if (GetWorld()->GetFirstPlayerController()->GetHitResultUnderCursor(ECC_Visibility, false, Hit))
	{
		float RotationYaw = FRotationMatrix::MakeFromX(Hit.ImpactPoint - GetActorLocation()).Rotator().Yaw;
		SetActorRotation(FRotator(0.0, RotationYaw, 0.0));
	}

}

// Called to bind functionality to input
void AMyCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent -> BindAxis(TEXT("MoveForvard"), this, &AMyCharacter::MoveForvard);
	PlayerInputComponent -> BindAxis(TEXT("MoveRight"), this, &AMyCharacter::MoveRight);

	PlayerInputComponent -> BindAction(TEXT("Jog"), IE_Pressed, this, &AMyCharacter::Jog);
	PlayerInputComponent -> BindAction(TEXT("Jog"), IE_Released, this, &AMyCharacter::Walk);

	PlayerInputComponent -> BindAction(TEXT("ZoomIn"), IE_Pressed, this, &AMyCharacter::ZoomIn);
	PlayerInputComponent -> BindAction(TEXT("ZoomOut"), IE_Pressed, this, &AMyCharacter::ZoomOut);

	PlayerInputComponent -> BindAction(TEXT("Sprint"), IE_Pressed, this, &AMyCharacter::StartSprint);
	PlayerInputComponent -> BindAction(TEXT("Sprint"), IE_Released, this, &AMyCharacter::StopSprint);

	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Pressed, this, &AMyCharacter::StartFire);
	PlayerInputComponent->BindAction(TEXT("Fire"), IE_Released, this, &AMyCharacter::StopFire);

	PlayerInputComponent -> BindAction(TEXT("Recharge"), IE_Pressed, this, &AMyCharacter::RechargeOn);
	
	PlayerInputComponent->BindAction(TEXT("Equip Weapon"), IE_Pressed, this, &AMyCharacter::EquipWeapon);	
	PlayerInputComponent->BindAction(TEXT("UnEquip Weapon"), IE_Pressed, this, &AMyCharacter::UnEquipWeapon);
	
// 	PlayerInputComponent->BindAction(TEXT("DropWeapon"), IE_Pressed, this, &AMyCharacter::DropWeapon);
}
