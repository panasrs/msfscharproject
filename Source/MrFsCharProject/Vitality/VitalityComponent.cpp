// Fill out your copyright notice in the Description page of Project Settings.


#include "VitalityComponent.h"
#include "TimerManager.h"
#include "Core/MrFs_GameInstance.h"

// Sets default values for this component's properties
UVitalityComponent::UVitalityComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UVitalityComponent::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner())
	{
		GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UVitalityComponent::DamageHandle);
	}
	
}


void UVitalityComponent::DamageHandle(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser)
{
	if (!IsAlive())
	{
		UE_LOG(LogTemp, Warning, TEXT("Already Dead"));
		return;
	}

	UE_LOG(LogTemp, Warning, TEXT("%s was damaged for %s"), *DamagedActor->GetName(), *FString::SanitizeFloat(Damage));
	
	CurrentHP -= Damage;

	OnHealthChanged.Broadcast();

	if (!IsAlive())
	{
		//Death
		OnOwnerDied.Broadcast();
		UE_LOG(LogTemp, Warning, TEXT("Owner Died"));

		UMrFs_GameInstance* MrFs_GameInstance = Cast<UMrFs_GameInstance>(GetWorld()->GetGameInstance());
		if (MrFs_GameInstance)
		{
		MrFs_GameInstance->AddScore(Score);
		}
	}
	else if (bCanRegenHP)
	{
		GetWorld()->GetTimerManager().ClearTimer(AutoRegenHP_TH);
		
		GetWorld()->GetTimerManager().SetTimer(AutoRegenHP_TH, this, &UVitalityComponent::RegenHP, RegenHPRate, true, RegenHPDelay);
	}
}

void UVitalityComponent::RegenHP()
{
	if (CurrentHP < MaxHP)
	{
		CurrentHP += RegenHPRate * RegenHPDelay;
		FMath::Clamp(CurrentHP, 0.f, MaxHP);
		UE_LOG(LogTemp, Warning, TEXT("%s HP regenerated"), *FString::SanitizeFloat(CurrentHP));
	}
	else
	{
		GetWorld()->GetTimerManager().ClearTimer(AutoRegenHP_TH);
		UE_LOG(LogTemp, Warning, TEXT("RegenHP Timer cleared"));
	}
	
}

void UVitalityComponent::StopRegenHP()
{
	CurrentHP = 0;
	GetWorld()->GetTimerManager().ClearTimer(AutoRegenHP_TH);
}

// Called every frame
void UVitalityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UVitalityComponent::GetMaxHP()
{
	return MaxHP;
}

float UVitalityComponent::GetCurrentHP()
{
	return CurrentHP;
}

void UVitalityComponent::SetCurrentHP(float HP)
{
	CurrentHP = HP;	
}

bool UVitalityComponent::IsAlive()
{
	return CurrentHP > 0; 
}



