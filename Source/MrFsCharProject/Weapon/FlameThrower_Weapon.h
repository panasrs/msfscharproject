// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Weapon/Base_Weapon.h"
#include "FlameThrower_Weapon.generated.h"

/**
 * 
 */
UCLASS(abstract)
class MRFSCHARPROJECT_API AFlameThrower_Weapon : public ABase_Weapon
{
	GENERATED_BODY()
	
public:
	virtual void Fire() override;

	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class AFlameThrower_Projectile> ProjectileClass;
};
