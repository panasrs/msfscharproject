// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon_Trace.h"
#include "CollisionQueryParams.h"
#include "WorldCollision.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Private/KismetTraceUtils.h"
#include "Base_Weapon.h"
#include "Weapon/ImpactEffectComponent.h"

void AWeapon_Trace::Fire()
{
	Super::Fire();

	TArray<FHitResult> Hits;

	FVector TraceStart = GetMuzzleTransform().GetLocation();
	FVector TraceEnd = GetMuzzleTransform().GetRotation().GetForwardVector() * TraceLength + TraceStart;

	FCollisionObjectQueryParams CollisionParams;
	CollisionParams.AddObjectTypesToQuery(ECC_PhysicsBody);
	CollisionParams.AddObjectTypesToQuery(ECC_WorldDynamic);
	CollisionParams.AddObjectTypesToQuery(ECC_WorldStatic);

	FCollisionShape CollisionShape = FCollisionShape::MakeSphere(SphereRadius);

	GetWorld()->SweepMultiByObjectType(Hits, TraceStart, TraceEnd, FQuat::Identity, CollisionParams, CollisionShape);

	DrawDebugLine(GetWorld(), TraceStart, TraceEnd, FColor::Red, true);

	if (Hits.Num() > 0)
	{
		for (auto TmpHit : Hits)
		{
		//	TmpHit.GetComponent()->AddImpulse(TmpHit.ImpactPoint);

			ImpactEffectComponent->SpawnImpactEffect(TmpHit);

			//DrawDebugSphere(GetWorld(), TmpHit.ImpactPoint, SphereRadius, 16, FColor::Blue, true);
		}
	}
}
