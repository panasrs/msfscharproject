// Fill out your copyright notice in the Description page of Project Settings.


#include "BAse_Projectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/StaticMeshComponent.h"
#include "ImpactEffectActor.h"
#include "Components/SphereComponent.h"

// Sets default values
ABAse_Projectile::ABAse_Projectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
// 	MovementComponent->InitialSpeed = 2000.f;
// 	MovementComponent->MaxSpeed = 2000.f;
// 	MovementComponent->bRotationFollowsVelocity = true;
// 	MovementComponent->ProjectileGravityScale = 0.2f;

	Bullet = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Bullet"));
	RootComponent = Bullet;

// 	ProjectileCollision = CreateDefaultSubobject<USphereComponent>(TEXT("ProjectileCollision"));
// 	ProjectileCollision->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);
// 	ProjectileCollision->SetSphereRadius(10.f);
// 	ProjectileCollision->BodyInstance.bNotifyRigidBodyCollision = true;
// 	ProjectileCollision->SetCollisionProfileName("BlockAll");
// 	ProjectileCollision->SetCanEverAffectNavigation(false);

// 	ProjectileMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh"));
// 	ProjectileMesh->SetupAttachment(RootComponent);
// 	ProjectileMesh->SetRelativeScale3D(FVector(0.05f, 0.05f, 0.05f));
// 	ProjectileMesh->SetCollisionProfileName("NoCollision");
// 	ProjectileMesh->SetGenerateOverlapEvents(false);
// 	ProjectileMesh->SetCanEverAffectNavigation(false);

	
}

// Called when the game starts or when spawned
void ABAse_Projectile::BeginPlay()
{
	Super::BeginPlay();

	SetLifeSpan(5);
	
	/** ignore our weapon and character  */
// 	if (Weapon && Weapon->GetOwner())
// 	{
// 		ProjectileCollision->IgnoreActorWhenMoving(Weapon->GetOwner(), true);
// 		ProjectileCollision->IgnoreActorWhenMoving(Weapon, true);
// 	}
}

// Called every frame
void ABAse_Projectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// void ABAse_Projectile::SetWeapon(class ABase_Weapon* NewWeapon)
// {
// 	Weapon = NewWeapon;
// }
// 
// void ABAse_Projectile::SetDamage(float NewDamage)
// {
// 	Damage = NewDamage;
// }

void ABAse_Projectile::NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);
	
	if (OnHitFire.IsBound())
	{
		OnHitFire.ExecuteIfBound(Hit);
	}
	
	Destroy();


}

