// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Base_Weapon.h"
#include "BAse_Projectile.generated.h"

DECLARE_DYNAMIC_DELEGATE_OneParam(FHitResultDelegate, FHitResult, Hit);

UCLASS()
class MRFSCHARPROJECT_API ABAse_Projectile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABAse_Projectile();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(VisibleAnywhere)
		class UProjectileMovementComponent* MovementComponent;

	UPROPERTY(VisibleAnywhere)
		class UStaticMeshComponent* Bullet;

	/* projectile sphere collision  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
		class USphereComponent* ProjectileCollision;

	/** static mesh component  */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Components", meta = (AllowPrivateAccess = "true"))
		class UStaticMeshComponent* ProjectileMesh;

// 	UPROPERTY(BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
// 		class ABase_Weapon* Weapon;

// 	UPROPERTY(BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
// 		float Damage = 1;
			
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

// 	UFUNCTION(BlueprintCallable)
// 		void SetWeapon(class ABase_Weapon* NewWeapon);
// 
// 		UFUNCTION(BlueprintCallable)
// 		void SetDamage(float NewDamage);

	UPROPERTY()
		FHitResultDelegate OnHitFire;

public:
	virtual void NotifyHit(class UPrimitiveComponent* MyComp, AActor* Other, class UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit) override;

};
