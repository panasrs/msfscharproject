// Fill out your copyright notice in the Description page of Project Settings.


#include "ImpactEffectComponent.h"
#include "ImpactEffectActor.h"
#include "Engine/World.h"

// Sets default values for this component's properties
UImpactEffectComponent::UImpactEffectComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UImpactEffectComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UImpactEffectComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UImpactEffectComponent::SpawnImpactEffect(FHitResult Hit)
{
	if (ImpactActor_Class)
	{
	FTransform SpawnTransform = FTransform::Identity;
	SpawnTransform.SetLocation(Hit.ImpactPoint);

	AImpactEffectActor* ImpactActor = GetWorld()->SpawnActorDeferred<AImpactEffectActor>(ImpactActor_Class, SpawnTransform, GetOwner());

	ImpactActor->HitInit(Hit);
	ImpactActor->FinishSpawning(SpawnTransform);
	}

}

