// Fill out your copyright notice in the Description page of Project Settings.


#include "FlameThrower_Projectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "ImpactEffectActor.h"
#include "Components/SceneComponent.h"
#include "Particles/ParticleSystemComponent.h"

// Sets default values
AFlameThrower_Projectile::AFlameThrower_Projectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	MovementComponent = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));

	FlameThrower = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("FlameThrower"));
 	RootComponent = FlameThrower;
}

// Called when the game starts or when spawned
void AFlameThrower_Projectile::BeginPlay()
{
	Super::BeginPlay();
	
	SetLifeSpan(5);
}

// Called every frame
void AFlameThrower_Projectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AFlameThrower_Projectile::NotifyHit(UPrimitiveComponent* MyComp, AActor* Other, UPrimitiveComponent* OtherComp, bool bSelfMoved, FVector HitLocation, FVector HitNormal, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::NotifyHit(MyComp, Other, OtherComp, bSelfMoved, HitLocation, HitNormal, NormalImpulse, Hit);

	Destroy();
}

