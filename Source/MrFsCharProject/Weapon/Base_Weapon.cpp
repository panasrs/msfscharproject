// Fill out your copyright notice in the Description page of Project Settings.


#include "Base_Weapon.h"
#include "MessageDialog.h"
#include "GameFramework/Character.h"
#include "ImpactEffectComponent.h"
#include "TimerManager.h"

ABase_Weapon::ABase_Weapon()
{
	GetStaticMeshComponent()->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	GetStaticMeshComponent()->SetMobility(EComponentMobility::Movable);

	ImpactEffectComponent = CreateDefaultSubobject<UImpactEffectComponent>(TEXT("ImpactEffectComponent"));


}

void ABase_Weapon::StartFire()
{
	//Fire();

	GetWorldTimerManager().SetTimer(AutoFire_TH, this, &ABase_Weapon::Fire, 60 / FireRate, bAutoFire, 0);
}

void ABase_Weapon::StopFire()
{
	GetWorldTimerManager().ClearTimer(AutoFire_TH);
}

void ABase_Weapon::Fire()
{
// 	if (LoadedAmmo <= 0)
// 	{
// 		return;
// 	}
// 	LoadedAmmo = LoadedAmmo - 1;

	if (FireMontage)
	{
		if (WeaponOwner)
		{
			WeaponOwner->PlayAnimMontage(FireMontage);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No Owner Found"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No Fire Montage Found"));
	}

	
// 	/*Do we have ammo in the AmmoPool?*/
// 	if (AmmoPool <= 0 || LoadedAmmo >= 30)
// 	{
// 		return;
// 	}
// 
// 	//Do we have enough to meet what the gun needs?
// 	if (AmmoPool < (30 - LoadedAmmo))
// 	{
// 		LoadedAmmo = LoadedAmmo + AmmoPool;
// 		AmmoPool = 0;
// 	}
// 	else
// 	{
// 		AmmoPool = AmmoPool - (30 - LoadedAmmo);
// 		LoadedAmmo = 30;
// 	}
}


void ABase_Weapon::Recharge()
{
	if (RechargeMontage)
	{
		if (WeaponOwner)
		{
			WeaponOwner->PlayAnimMontage(RechargeMontage);
		}
		else
		{
			UE_LOG(LogTemp, Warning, TEXT("No Owner Found"));
		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("No Recharge Montage Found"));
	}
}

FTransform ABase_Weapon::GetMuzzleTransform()
{
	if (MuzzleSocketName == "None")
	{
	FMessageDialog::Debugf(NSLOCTEXT("Error", "Key", "MuzzleSocketName is not initialized!"));
	return FTransform::Identity;
	}
	
	return GetStaticMeshComponent()->GetSocketTransform(MuzzleSocketName);
	
}

void ABase_Weapon::BeginPlay()
{
	Super::BeginPlay();

	if (GetOwner())
	{
		WeaponOwner = Cast<ACharacter>(GetOwner());
		if (!WeaponOwner)
		{
			UE_LOG(LogTemp, Warning, TEXT("Owner is not Character"));
		}
	}
}
