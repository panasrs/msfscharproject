// Fill out your copyright notice in the Description page of Project Settings.


#include "ThrowableWeapon.h"
#include "Granade_Projectile.h"
#include "ConstructorHelpers.h"
#include "Engine/World.h"
#include "BAse_Projectile.h"
#include "MyCharacter.h"

AThrowableWeapon::AThrowableWeapon()
{
	WeaponMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("WeaponMesh"));
	SetRootComponent(WeaponMesh);
	WeaponMesh->SetRelativeScale3D(FVector(0.2f, 0.2f, 0.2f));

	static ConstructorHelpers::FObjectFinder<UStaticMesh> Sphere(TEXT("StaticMesh'/Engine/BasicShapes/Sphere.Sphere'"));
	if (Sphere.Succeeded())
	{
		WeaponMesh->SetStaticMesh(Sphere.Object);
	}

	ThrowProjectileClass = AGranade_Projectile::StaticClass();
}

void AThrowableWeapon::ThrowProjectile()
{
	GetWorld()->GetTimerManager().ClearTimer(ThrowProjectileTimer);

	if (AMyCharacter* MyCharacter = Cast<AMyCharacter>(GetInstigator()))
	{
		if (ThrowProjectileClass && GetWorld())
		{
			FActorSpawnParameters SpawnParams;
			SpawnParams.Owner = this;
			SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			FVector SpawnLocation;
			FRotator SpawnRotation;
			SpawnLocation += (FVector(0.f, 0.f, 40.f) + GetActorForwardVector() * 50.f);

			ABAse_Projectile* SpawnedProjectile = GetWorld()->SpawnActor<ABAse_Projectile>(ThrowProjectileClass, SpawnLocation, SpawnRotation, SpawnParams);
		
			if (SpawnedProjectile)
			{
// 				SpawnedProjectile->SetWeapon(this);
// 				SpawnedProjectile->SetDamage(Damage);
				if (LoadedAmmo <= 0)
				{
// 					WeaponOwner->DropWeapon(this);
				}
			}
		}
	}
}

void AThrowableWeapon::UseWeapon()
{
	if (bMustHoldToThrow)
	{
		if (!ThrowProjectileTimer.IsValid())
		{
			GetWorldTimerManager().SetTimer(ThrowProjectileTimer, this, &AThrowableWeapon::ThrowProjectile, HoldTime, false);
		}
	}
	else
	{
		ThrowProjectile();
	}

}

void AThrowableWeapon::StopUseWeapon()
{
	GetWorld()->GetTimerManager().ClearTimer(ThrowProjectileTimer);
}
