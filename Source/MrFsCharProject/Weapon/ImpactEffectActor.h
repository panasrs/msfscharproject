// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ImpactEffectActor.generated.h"

UCLASS()
class MRFSCHARPROJECT_API AImpactEffectActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AImpactEffectActor();

	void HitInit(FHitResult Hit);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class UMaterialInterface* DecalMaterial;
	
	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class USoundBase* EffectSound;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		class UParticleSystem* EffectParticle;

	UPROPERTY(EditDefaultsOnly, Category = "Effects")
		bool bApplyImpulse = true;

	UPROPERTY(EditDefaultsOnly, Category = "Effects", meta = (EditCondition = "bApplyImpulse"))
		float ImpulseStrength = 300;

	UPROPERTY(EditDefaultsOnly, Category = "Damage")
		float Damage = 20;

	FHitResult EffectHit;
	
	void SpawnEffects();
	
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
