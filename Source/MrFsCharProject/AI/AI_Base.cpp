// Fill out your copyright notice in the Description page of Project Settings.


#include "AI_Base.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Vitality/VitalityComponent.h"
// #include "GameFramework/Pawn.h"

// Sets default values
AAI_Base::AAI_Base()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCharacterMovement()->bOrientRotationToMovement = false;
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 600.0f, 0.0f);

	VitalityComponent = CreateDefaultSubobject<UVitalityComponent>(TEXT("VitalityComponent"));

// 	VitalityComponent->OnOwnerDied.AddDynamic(this, &AAI_Base::BotDied);
}

// void AAI_Base::BotDied()
// {
// 	//GetController()->UnPossess();
// 	
// }

// Called when the game starts or when spawned
void AAI_Base::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAI_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void AAI_Base::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

