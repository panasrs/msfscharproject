// Fill out your copyright notice in the Description page of Project Settings.


#include "AI_Waypoint.h"
#include "AI_Base.h"

// Sets default values
AAI_Waypoint::AAI_Waypoint()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Box Component"));
	BoxComponent->SetupAttachment(GetRootComponent());
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AAI_Waypoint::OnPlayerEnter);
}

// Called when the game starts or when spawned
void AAI_Waypoint::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAI_Waypoint::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AAI_Waypoint::OnPlayerEnter(UPrimitiveComponent* OverlapComponent, AActor* OtherActor, 
	UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFrowSweep, const FHitResult& SweepResult)
{
	AAI_Base* Character = nullptr;

	if (OtherActor != nullptr)
	{
		Character = Cast<AAI_Base>(OtherActor);
		if (Character != nullptr)
		{
			Character->NextWaypoint = NextWaypoint;
		}
	}
}

