// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "AI_Base.generated.h"

UCLASS()
class MRFSCHARPROJECT_API AAI_Base : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AAI_Base();

// 	void AttachWeapon(FName SocketName);

// 	UFUNCTION()
// 		void BotDied();


	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class AAI_Waypoint* NextWaypoint;	

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		class UVitalityComponent* VitalityComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
