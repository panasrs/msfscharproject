// Fill out your copyright notice in the Description page of Project Settings.


#include "MrFs_GameInstance.h"

int UMrFs_GameInstance::GetCurrentScore() const
{
	return CurrentScore;
}

void UMrFs_GameInstance::AddScore(int ScoreToAdd)
{
	CurrentScore += ScoreToAdd;
}

void UMrFs_GameInstance::FlushCurrentScore()
{
	CurrentScore = 0;
}
