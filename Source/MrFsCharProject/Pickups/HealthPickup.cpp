// Fill out your copyright notice in the Description page of Project Settings.


#include "HealthPickup.h"
#include "MyCharacter.h"
#include "Vitality/VitalityComponent.h"

void AHealthPickup::NotifyActorBeginOverlap(AActor* OtherActor)
{
	Super::NotifyActorBeginOverlap(OtherActor);

// 	AMyCharacter* MyCharacter = Cast<AMyCharacter>(OtherActor);
	
}

void AHealthPickup::HealthPickup(UPrimitiveComponent* OverlapperComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	UVitalityComponent* VitalityComponent = Cast<UVitalityComponent>(GetComponentByClass(UVitalityComponent::StaticClass()));

	if (OtherActor->GetComponentByClass(UVitalityComponent::StaticClass()))
	{
		float HP = (VitalityComponent->GetCurrentHP() + RecoverHealth, VitalityComponent->GetMaxHP());
		VitalityComponent->SetCurrentHP(HP);
		UE_LOG(LogTemp, Warning, TEXT("%s recovered"));
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("HealthPickup didn't find UVitalityComponent"));
	}
}
