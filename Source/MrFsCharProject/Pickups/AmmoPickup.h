// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AmmoPickup.generated.h"


UCLASS()
class MRFSCHARPROJECT_API AAmmoPickup : public AActor
{
	GENERATED_BODY()

	
public:	
	// Sets default values for this actor's properties
	AAmmoPickup();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
		class USceneComponent* SceneComponent;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Ammo Pickup")
		class USphereComponent* TouchSphere;

	UPROPERTY(VisibleDefaultsOnly, BlueprintReadOnly, Category = "Ammo Pickup")
		class UStaticMeshComponent* StaticMesh;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Ammo Pickup")
		int32 count;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
		FRotator RotationRate;

	//Rotation speed
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Pickup")
		float Speed;

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlapperComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComponent,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
};
