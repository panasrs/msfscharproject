// Fill out your copyright notice in the Description page of Project Settings.


#include "AmmoPickup.h"
#include "GameFramework/Character.h"
#include "Components/SphereComponent.h"
#include "Components/StaticMeshComponent.h"
#include "MyCharacter.h"
#include "Components/PrimitiveComponent.h"
#include "Components/SceneComponent.h"

// Sets default values
AAmmoPickup::AAmmoPickup()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("SceneComponent"));
	RootComponent = SceneComponent;

	StaticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMeshComponent"));
	StaticMesh->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	TouchSphere = CreateDefaultSubobject<USphereComponent>(TEXT("TouchSphereComponent"));
	TouchSphere->SetGenerateOverlapEvents(true);
	TouchSphere->SetSphereRadius(50, false);
	TouchSphere->OnComponentBeginOverlap.AddDynamic(this, &AAmmoPickup::OnOverlapBegin);
	TouchSphere->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetNotIncludingScale);

	RotationRate = FRotator(0.0, 180.0, 0.0);

	Speed = 1.0;

	count = 30;
}

// Called when the game starts or when spawned
void AAmmoPickup::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AAmmoPickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	AddActorLocalRotation(RotationRate * DeltaTime * Speed);
}

void AAmmoPickup::OnOverlapBegin(UPrimitiveComponent* OverlapperComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	AMyCharacter* MyCharacter = Cast<AMyCharacter>(OtherActor);

	if ((OtherActor != nullptr) && (OtherActor != this) && (OtherComponent != nullptr))
	{
		MyCharacter->AmmoPool = MyCharacter->AmmoPool + count;

		this->Destroy();
	}
}

